package com.client.musicplayer;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class CompositionsList extends ListFragment {

    private OnFragmentInteractionListener mListener;

    public static CompositionsList newInstance() {
        CompositionsList fragment = new CompositionsList();
        return fragment;
    }

    public CompositionsList() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }

        ArrayList<Item> list = new ArrayList<Item>();
        list.add(new Item(R.raw.nikelback, "Nickelback"));
        list.add(new Item(R.raw.tdg, "Three Days Grace"));
        list.add(new Item(R.raw.pg, "Piano Guys"));

        setListAdapter(new ArrayAdapter<Item>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, list));
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            mListener.onFragmentInteraction(((Item)(getListAdapter().getItem(position))).id+"");
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    public class Item{
        public int id;
        public String name;

        public Item(int id, String name){
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString(){
            return this.name;
        }
    }

}
