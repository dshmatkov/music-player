package com.client.musicplayer;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.zip.Inflater;


public class Player extends Fragment implements GestureOverlayView.OnGesturePerformedListener{

    private OnFragmentInteractionListener mListener;
    private Activity activity;
    private boolean isPlaying = false;
    private static int currentCompId = R.raw.nikelback;
    private static boolean songChanged = true;
    private GestureLibrary mGestureLib;
    private ImageButton playPause;

    public static Player newInstance(Activity activity) {
        Player fragment = new Player();
        fragment.activity = activity;
        return fragment;
    }

    public Player() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, container, false);
        ImageButton button = (ImageButton)view.findViewById(R.id.play_pause);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.this.onPlayClick(v);
            }
        });
        playPause = button;

        mGestureLib = GestureLibraries.fromRawResource(this.getContext(), R.raw.gesture);
        mGestureLib.load();

        GestureOverlayView gestures = (GestureOverlayView)view.findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(this);

        return view;
    }


    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture){
        ArrayList<Prediction> predictions = mGestureLib.recognize(gesture);
        if (predictions.size() > 0){
            Prediction prediction = predictions.get(0);
            if (prediction.score > 1.0){
                if (prediction.name.equals("play") && !this.getIsPlaying()) {
                    this.onPlayClick(this.playPause);
                }
                else if(prediction.name.equals("pause") && this.getIsPlaying()){
                    this.onPlayClick(this.playPause);
                }
            }
        }
    }

    public void onPlayClick(View view){
        //if (this.activity == null) return;
        Intent intent = new Intent(this.activity, MediaService.class);
        intent.putExtra("c", currentCompId);
        if(!this.isPlaying && currentCompId != 0){
            System.out.println("Try to start service.");
            this.isPlaying = true;
            view.setBackgroundResource(R.drawable.pause);
            if (songChanged) {
                this.activity.startService(intent);
                songChanged = false;
            }else{
                MediaService.ambientMediaPlayer.start();
            }
        }
        else{
            this.isPlaying = false;
            view.setBackgroundResource(R.drawable.play);
            if (MediaService.ambientMediaPlayer != null) {
                MediaService.ambientMediaPlayer.pause();
            }
        }
    }

    public void setCurrentCompId(int id){
        currentCompId = id;
        songChanged = true;
        //onPlayClick(playPause);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        //public void onFragmentInteraction(Uri uri);
    }

    public boolean getIsPlaying(){
        return isPlaying;
    }

}
